## Пример использования

```
❯ python main.py -m E0:D5:5E:1E:91:98
[*] Поиск производителя для: e0d5-5e1e-9198
[+] Производитель: GIGA-BYTE TECHNOLOGY CO.,LTD.
[+] Адрес производства: Pin-Jen City, Taoyuan, Taiwan, R.O.C. Pin-Jen Taoyuan TW 324 
```
## Офлайн обновление баз

для обновления баз нужно скачать три файла [отсюда](https://regauth.standards.ieee.org/standards-ra-web/pub/view.html)
- oui36.csv
- mam.csv
- oui.csv