import csv
import argparse
from sys import exit


class InvalidMacError(Exception):
    pass


def get_arguments():
    # Метод для получения аргументов из командной строки
    parser = argparse.ArgumentParser()

    # получаем MAC адрес в аргументе
    parser.add_argument("-m", "--macaddress",
                        dest="mac_addr",
                        help="MAC адрес устройства"
                        )
    options = parser.parse_args()

    # Проверка получения аргумента
    if options.mac_addr:
        return options.mac_addr
    else:
        parser.error("[!] Ошибка ввода. "
                     "Используй --help для получения справки.")


def get_normalize_mac(mac):
    """Приведение MAC адреса к виду ABCDEF123456, и проверка его корректности"""
    mac_address_normal = mac.replace(':', '').replace('-', '').replace('.', '').upper()
    try:
        int(mac_address_normal, 16)
    except ValueError:
        raise InvalidMacError("{} содержит не поддерживаемый символ".format(mac))
    if len(mac_address_normal) > 12:
        raise InvalidMacError("{} слишком много символов".format(mac))
    if len(mac_address_normal) < 12:
        raise InvalidMacError("{} не хватает символов".format(mac))
    return mac_address_normal


def get_huawei_mac(mac):
    """Приведение MAC адреса к виду abcd-ef12-3456, для отображения"""
    return (mac[:4] + '-' + mac[4:8] + '-' + mac[-4:]).lower()


def get_mac_details(mac):
    """
    Поиск MAC адреса в csv-файлах с данными
    скаченны из официального сайта
    https://regauth.standards.ieee.org/standards-ra-web/pub/view.html
    """
    with open('oui36.csv') as f:
        reader = csv.DictReader(f)
        for row in reader:
            if row['Assignment'] in mac:
                return row['Organization Name'], row['Organization Address']
    with open('mam.csv') as f:
        reader = csv.DictReader(f)
        for row in reader:
            if row['Assignment'] in mac:
                return row['Organization Name'], row['Organization Address']
    with open('oui.csv') as f:
        reader = csv.DictReader(f)
        for row in reader:
            if row['Assignment'] in mac:
                return row['Organization Name'], row['Organization Address']
    return None, None


if __name__ == "__main__":
    mac_address_raw = get_arguments()

    try:
        mac_address = get_normalize_mac(mac_address_raw)
    except InvalidMacError as err:
        exit('[!] Не корректный MAC адрес: {}'.format(err))

    print('[*] Поиск производителя для: {mac}'.format(mac=get_huawei_mac(mac_address)))

    vendor_name, vendor_address = get_mac_details(mac_address)

    if vendor_name is None:
        exit('[!] {} не найден'.format(mac_address) + ' попробуйте обновить данные')

    print("[+] Производитель: " + vendor_name)
    print("[+] Адрес производства: " + vendor_address)
